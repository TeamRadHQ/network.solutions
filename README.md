#Network Solutions

This is the repository for the Network Solution web design. 

[View a live demonstration](http://teamradhq.com/network.solution/)

[View client brief](https://bitbucket.org/TeamRadHQ/network.solutions/raw/77001c023a736b36ff2b96db2c45fdb62369f2d1/assets/clientBrief.pdf)

##Design

The design for this site uses a complex, single viewport, flexible layout. Page layout is refactored using a combination of CSS media queries and JavaScript.

![Desktop Layout](/img/desktop.png)
![Mobile Layout](/img/phone.png)

###Interactions

A number of interactions have been scripted using a combination of jQuery and JavaScript.

####Slide Show

Once the page is loaded, an unordered list is added to the page header which contains a set of images. A slideshow is then created using jQuery to cycle through and display each element. 

####Lightbox

Clicking items on the main site navigation will display a popup lightbox with additional content. 

#### Flexible Layout

Most of the flexible layout is styled using CSS media queries. However, some JavaScript is required to ensure a consistent appearance across a range of viewports. 

For example:

![Navigation bar example](/img/navbar.png)

* Text on the article navigation is replaced with icons on smaller using media queries.

* The arrow next to the active article nav item is added and removed using JavaScript.


Additionally, the article content, navigation items and lightbox are resized using JavaScript to ensure that the page elements are always constrained to a single viewport.


##CSS 3

CSS for this site has been generated using the [Gulp](http://gulpjs.com) build system. Styles have been written using the [Stylus](http://stylus-lang.com) preprocessor. 

The CSS is further process using [PostCSS](http://postcss.org) plugins. [Autoprefixer](https://github.com/postcss/autoprefixer) improves cross-browser compatibility and [Clean-CSS](https://github.com/jakubpawlowicz/clean-css) reduces the size of the CSS by merging style rules and minifying output. 

##Custom Polymer Elements

The [<google-map> custom element set](https://elements.polymer-project.org/elements/google-map) from Google's [Polymer Project](https://www.polymer-project.org/1.0/) have been used to integrate Google Maps. 