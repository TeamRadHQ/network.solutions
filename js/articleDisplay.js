$(document).ready(function() {
var appCounter = 0;
/**
 * Application Variables
 */
var doc = document, 
	win = window;
// Section for hacked arrow styles.
var styles = doc.getElementsByTagName('style');
	styles = styles[0];

// Page content and main page elements.
var $body = $('body'),
 	$mainHeader = $body.children('header'), 	// Page header
 	$mainNav    = $body.children('nav'), 		// Main navigation area
 	$navList 	= $mainNav.children('ul'),		// The actual nav items
 	$main       = $body.children('main'), 		// Main content area
 	$articleNav = $main.find('nav').children(), // Content navigation
 	$article    = $main.find('article'), 		// All page content
 	$sections   = $article.find('section'); 	// Page content sections	

/**
 * Application Event handlers
 */
// Document loader
window.onload = docLoader();

// Resize event
$(window).resize(resize);

// Click event for displaying article sections
$articleNav.children().click(function(){
	displaySection('.' + this.className);
});
// Lightbox click event.
$mainNav.find('li').each(function(){
	var className = this.className;
	this.onclick = function(){
		displayLightbox(className);	
	};
});

/**
 * Resize page and displays content
 * @return {void}
 */
function docLoader(){
	resize();
	displayLoader();
}

/**
 * Waits for screen to stop resizing then fires resize methods.
 * @return {void}
 */
var resizeTimer;
function resize(){
	// debugCounter(arguments.callee.name);
	clearTimeout(resizeTimer);
	resizeTimer = setTimeout(function(){
		resizeMainNav();
		resizeArticle();
		resizeLightbox();
	}, 350);
}

/**
 * Removes main nav to a dropdown menu or resizes for large screen.
 * @return {void}
 */
function resizeMainNav() {
	// debugCounter(arguments.callee.name);

	hideMainNavList(); // Hide nav items.
	var navListWidth,
		navInputWidth;
	var button = toggleNavButton();
	// Calculate nav dimensions
	if(win.innerWidth <= 768) { // Smaller screens
		navListWidth = window.innerWidth - 20;
		navInputWidth = window.innerWidth - button.width() - 60;
	} else { // Larger screens
		navListWidth = '7.5rem';
		navInputWidth = $mainNav.width()/3;
	}
	resizeNavInput(navInputWidth);
	resizeMainNavListItems(navListWidth);
	showMainNavList();

} // End resizeMainNav()
/**
 * Resizes main nav items if not already of size width.
 * @param  {integer} width The width you want to resize to
 * @return {void}
 */
function resizeMainNavListItems(width) {
	if ($navList.children().width() !== width) {
		$navList.children().width(width);
	}
}
/**
 * Resizes main nav inputs if not already of size width.
 * @param  {integer} width The width you want to resize to.
 * @return {void}       
 */
function resizeNavInput(width) {
	$mainNav.find('form').each(function(){
		if ($(this).children().width() !== width) {
			// debugCounter(arguments.callee.name);
			$(this).children().width(width);
		}
	});
}
/**
 * Resizes the article navigation, then calculates the 
 * article height and resizes visible contnet.
 * @return {void}
 */
function resizeArticle() {
	// Check window and calculate article height
	var articleHeight = mainHeight();
	resizeArticleNav(articleHeight);
	// Set the height of the article viewport.
	if (win.innerWidth > 768) {
		sectionHeight = articleHeight;
	} else {
		sectionHeight = articleHeight - $articleNav.height();
	}
	resizeArticleHeight(sectionHeight);
}

/**
 * Sets the article viewport and visible content to height
 * if it is not already.
 * @param {integer} height The height in pixels.
 */
function resizeArticleHeight(height) {
	if (parseInt($article[0].style.height) != height) {
		// Resize the article viewport
		$article[0].style.height = height + 'px';
		$article.children().each(function(){
			// Resize the displayed section
			if (this.style.display !== 'none') {
				this.css = {
					'backgroundColor' : 'white',
					'min-height' : height + 'px',
					'max-height' : height + 'px'
				};
			}
		});
	}
}
/**
 * Resizes the article nav list based on its position and article height.
 * @param  {integer} navHeight Calculated article height.
 * @return {void}
 */
function resizeArticleNav(mainHeight) {
	var navWidth ='', 
		navHeight, 
		navItemHeight, 
		arrowClass = '';

	// Calculate nav item width and heights from screen width.	
	if (win.innerWidth > 768) {
		navHeight = mainHeight;
		navItemHeight = mainHeight / $articleNav.children().length;
		arrowClass = 'arrow';
	} else {
		navWidth = win.innerWidth / $articleNav.children().length;
		navHeight = mainHeight/8;
		navItemHeight = mainHeight / ($articleNav.children().length*2);
	}

	// Set minimum height for small items.
	if(navHeight < 50) { 
		navHeight = 50; 
		navItemHeight = 50; 
	}

	// Resize the article nav if size has changed.

	resizeArticleNavList(navHeight);
	resizeArticleNavListItems(navWidth, navItemHeight, arrowClass);
	arrowStyles(navItemHeight);
}
/**
 * Resizes the article nav to height if it is changed.
 * @param  {integer} height The height in pixels.
 * @return {void}
 */
function resizeArticleNavList(height) {
	if ($articleNav.height() !== height) {
		$articleNav.height(height);
	}	
}
/**
 * Checks to see if article nav list items need to be resized.
 * @param  {integer} width     The width in pixels
 * @param  {integer} height    The height in pixels
 * @param  {string} className A class to append to each item.
 * @return {void}
 */
function resizeArticleNavListItems(width, height, className) {
	if (width != $articleNav[0].style.width || height != $articleNav[0].style.height) {
		// Resize each article nav item.
		$articleNav.children('li').each(function(){
			$(this).width(width);
			this.style.maxWidth = width + 'px';
			$(this).height(height);
			this.style.lineHeight = height + 'px';
			$(this).addClass(className);
		});	
	}
}

/**
 * Resizes the lightbox, if it exists.
 * @return {void}
 */
function resizeLightbox() {	
	lightbox = $('.lightbox'); // Get the lighbox
	content = lightbox.find('.content');
	if(lightbox.length) { // Lightbox exists
		content.height($(window).height() - 100); // Set the content height
		content.children('section')[0].style.overflow = 'auto';
		if($('network-map').length) { // Resize google map if it exists.
			resizeMap(content);
		} else {
			content.children('section')[0].style.maxHeight = content.height() - 50 + 'px';
			content.children('section')[0].style.height = content.height() - 50 + 'px';
		}
	}
}
/**
 * Removes and redisplays google map if it exists.
 * @param  {jQuery} content The content the map belongs to.
 * @return {void}         
 */
function resizeMap(content) {
	var map = $('network-map');
	// Resize if the map exists and height is changed.
	if( map.height() !== mapHeight()) { 
		map.remove();
		setTimeout(getMap(content), 150);
	}
}
/**
 * Hides the main nav list, if it's visible.
 * @return {void}
 */
function hideMainNavList() {
	if ( $navList.is(':visible') ) {
		// debugCounter(arguments.callee.name);
		$navList.hide();
	}
}
/**
 * Shows the man nav list, if it's invisible.
 * @return {void}
 */
function showMainNavList() {
	if ( ! $navList.is(':visible') && window.innerWidth > 768 ) {
		// debugCounter(arguments.callee.name);
		$navList.show();
	}	
}
/**
 * Adds or removes main nav button, depending on screen size.
 * @return {jQuery} The button element created/removed from page.
 */
function toggleNavButton() {

	var button = $mainNav.find('button');
	if(win.innerWidth <= 768) { // Smaller screens
		if (! button.length) { // Add button if it doesn't exist.
			// debugCounter(arguments.callee.name);
			button = $('<button/>', {
				text: 'i',
				click: function(){$navList.slideToggle();}
			});
			$mainNav.append(button);			
		}
	} else { // Larger screens
		if (button.length) { // Remove button if it exists.
			// debugCounter(arguments.callee.name);
			button.remove();
		}
	}
	return button;
}
/**
 * Displays the supplied section or default.
 * @return {void}
 */
function displayLoader() {
	// debugCounter(arguments.callee.name);
	// Hide ALL article sections by default
	$sections.each(function(){ this.style.display="none"; });
	// Display hashed section, or default.
	var hash = getHash();
	if (hash.length > 0 && $sections.hasClass(hash)) {
		displaySection('.' + hash);
	} else {
		displaySection('.' + $sections[0].className);	
	}
}
/**
 * Displays the article section having className
 * @param  {string} className '.className'
 * @return {void}           Manipulates DOM.
 */
function displaySection(className) {

	className = stripContext(className);
	
	// Get the item selected for display
	var selection = $sections.filter(className);

	// Only show/hide if the selection is hidden
	if( selection[0].style.display == 'none') {
		// debugCounter(arguments.callee.name);
		// Fade out all sections
		$sections.fadeOut(0); 
	
		// Add / Remove active class to article nav.
		$articleNav.children().removeClass('active');
		$articleNav.children().filter(className).addClass('active');
		selection.filter(className).fadeIn(300); // Fade in selected
		setHash( className.replace('.','#') ); // Update the page anchor

		// Clear HTML, wait and repopulate for scrolling
		var html = selection[0].innerHTML;
		selection[0].innerHTML = "";
		setTimeout(function(){
			selection[0].innerHTML = html;
		}, 100);
	} // endif
	return;
}
/**
 * Shows the lightbox with content for className.
 * @param  {className} className This is the nav className which corresponds to content.
 * @return {void}           
 */
function displayLightbox(className) {
	lightbox = makeLightbox(className);
	// Add content to lightbox and add to body
	lightbox.appendTo('body').fadeIn(450);
	// Add map for locations lightbox
	if (className == 'locations') {
		getMap(content);
	}
	resizeLightbox();
}
/**
 * Hides lightboxes and removes from document
 * @return {void}
 */
function hideLightbox() {
	// debugCounter(arguments.callee.name);
	lightbox = $body.find('#lightbox');
	lightbox.children().fadeOut(450);
	lightbox.fadeOut(550);
	setTimeout(function(){
		lightbox.remove();
	}, 550);
}

function makeLightbox(className) {
	// debugCounter(arguments.callee.name);

	// Concatenate the section's id and clone inner content.
	var lightboxId = '#' + className + 'Lightbox',
		innerContent = $article.find(lightboxId).addClass('innerContent').clone().show();
	// Create title and add to innerContent
	title = $('<h2>', {
		text: className
	});
	innerContent.prepend(title);

	// Create a lighbtbox div
	var lightbox = $('<div/>',{
    	id: 'lightbox',
    	class: 'lightbox',
    	click: function(e) { // Doesn't fire on children.
  			if (e.target !== this) { return; }
    		hideLightbox();
		},
		css: {
			'background-color' :'rgba(58,58,58,0.85)',
			'display' : 'none'
		}    	
	});
	// Create close button
	button = $('<button>', {
		click: hideLightbox,
		text: 'X'		
	});
	// Create a section for content.
	content = $('<section/>',{
		class:'content',
	});
	// Add button, title, content and inner
	content.append(button).append(innerContent);
	content.height($(window).height() - 100);
	lightbox.append(content);
	return lightbox;
}

/**
 * Adds a map to the supplied content.
 * @param {jQuery} content The element you're appending to.
 */
function getMap(content) {
	// Create a new map element.
	map = $('<network-map height="'+mapHeight()+'"></network-map>');
	content.append(map);
}
/**
 * Calculates the height of the main content area.
 * @return {integer} The current height of the content area.
 */
function mainHeight() {
	var mainHeight = win.innerHeight - $mainHeader.height() - $mainNav.height();
	return mainHeight;
}
/**
 * Calculates the display height for maps.
 * @return {integer} The height for the map.
 */
function mapHeight() {
	console.log(content.children('.innerContent').height());
	console.log(content.height());
	return content.height() - content.children('.innerContent').height() - 50;
}

/**
 * Adds or removes pseudo-element declarations for article nav items.
 * @param  {integer} navItemHeight The current height of each item.
 * @return {integer}               The calculated arrow height.
 */
function arrowStyles(navItemHeight) {
	var arrowHeight, arrow, arrowActive, arrowHover;
	// Create style rules for arrows on widescreens.
	if (win.innerWidth > 768) {
		// Calculate height of arrow sides
		arrowHeight = navItemHeight / 2;
		// Style declarations for arrows. (Note: inactive arrows disabled)
		arrow  = 'border-top: '+ arrowHeight +'px solid transparent;';
		arrow += 'border-bottom: ' + arrowHeight + 'px solid transparent;';
		arrow += 'border-left: ' + arrowHeight + 'px solid #b0b0b0;';
		arrow += 'right: ' + -(arrowHeight)+1 + 'px;';
		arrow += 'width: ' + (arrowHeight)+1 + 'px;';
		arrowActive = 'border-left: ' + arrowHeight + 'px solid #10fefe;';
		arrowHover  = 'border-left: ' + arrowHeight + 'px solid #64fefe;';
	} else {
		arrow = 'border:none;';
		arrowActive = 'border:none;';
		arrowHover = 'border:none;';
	}
	// Replace rules in head > style
	styles.innerHTML = '.arrow:after{'+arrow+'}';
	styles.innerHTML+= '.active:after{'+ arrowActive +'}';
	styles.innerHTML+= '.arrow:hover:after{'+  arrow +  arrowHover +'}';
	
	// Pad articles 
	padArticle(arrowHeight);
	
	return arrowHeight;
}

/**
 * Pads article content by padding on wider screens or 10px.
 * @param  {integer} padding The calculated padding to apply.
 * @return {void}         
 */
function padArticle(padding) {
	// debugCounter(arguments.callee.name);
	if (win.innerWidth > 768) {
		$article.children().each(function(){
			this.style.paddingLeft = padding + 30 + 'px';
			this.style.paddingRight = padding / 2 + 'px';
		});
	} else {
		$article.each(function(){
			if (this.style.display != 'none') {
				this.style.width = '100%';
				$(this).children().each(function(){
					this.style.paddingLeft = '10px';
					this.style.paddingRight = '10px';
				});				
			}
		});
	}
}
/**
 * Strips any contextual classes from an element's className.
 * @param  {string} className The element's className
 * @return {string}           The stripped className
 */
function stripContext(className) {
	// Remove contextual classes
	return className.replace(' arrow', '')
					.replace(' active', '');
}
/**
 * Removes (#) and returns a plain text string from the URL
 * @return {string} The value of the URL's hash.
 */
function getHash() {
	return win.location.hash.replace('#','');
}
/**
 * Sets hash to the URL.
 * @param {string} hash The value of the hash you want to set.
 */
function setHash(hash) {
	win.location.hash = hash;
	return;
}

function debugCounter(calleeName) {
	console.log(calleeName + ': ' + appCounter);
	appCounter++;
}

}); // End document.ready