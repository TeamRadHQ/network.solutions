$(document).ready(function() {
var SlideShow = {
	count: 7, // 0 index
	current: 0,
	duration: 6000,
	timer: null,
	slides: $('body > header > ul'),
	list: $('<ul>'),
	addSlides : function(){
		for (ii=0; ii<this.count; ii++) {
			this.list.append('<li style="display:none">'+ii+'</li>');
		}
		$('body > header').append(this.list);
		this.setSlides();
	},
	setSlides: function(){ 
		this.slides = $('body > header > ul > li');
	},
	show: function() { 
		// Get the current slide and fade in.
		slide = SlideShow.slides.eq(SlideShow.current);
		slide.fadeIn(0.25 * SlideShow.duration);
		// Set a timer for fadeout.
		this.timer = setTimeout(function(){		
			slide.fadeOut(0.5 * SlideShow.duration);

			if (SlideShow.current < SlideShow.count) {
				SlideShow.current++;
			} else {
				SlideShow.current=0;
			}
			SlideShow.show(); // Call self
		}, this.duration);
	}
};
SlideShow.addSlides();
SlideShow.show();

}); // End document.ready