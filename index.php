
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
    <meta name="Author" content="Paul Beynon">

    <!-- Polymer -->
    <!-- Polyfill Web Components support for older browsers -->
    <script src="bower_components/webcomponentsjs/webcomponents.min.js"></script>
    <script src="bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>

    <!-- Import elements -->
    <link rel="import" href="elements/network-map.html">
	<title>Network Solutions</title>
	<link rel="stylesheet" href="css/app.css">

	<script src="https://code.jquery.com/jquery-2.2.2.min.js" integrity="sha256-36cp2Co+/62rEAAYHLmRCPIych47CvdM+uTBJwSzWjI=" crossorigin="anonymous"></script>
	<script src="js/articleDisplay.js"></script>
	<script src="js/SlideShow.js"></script>

	<style>	
	</style>
</head>
<body>
<header>
	<h1>Network Solution</h1>
</header>
<main>
	<article>
		<!-- <a name="networking"></a> -->
		<section class="networking">
			<img src="img/media/icon-network-01.png">
			<h2>Networking</h2>
			<p>DIY put a bird on it thundercats mlkshk vice selvage, flexitarian celiac single-origin coffee. Actually jean shorts blog cold-pressed celiac master cleanse. Paleo plaid hoodie, mumblecore kinfolk beard listicle typewriter brooklyn normcore. Pinterest asymmetrical blog, fashion axe man bun bushwick tattooed hoodie austin +1 next level. Fixie kombucha four loko ugh. Gentrify try-hard bitters paleo freegan pop-up. Plaid cornhole echo park franzen, 90's scenester wayfarers.</p>
			<p>Ennui street art knausgaard, messenger bag swag freegan next level viral brunch cronut. Wolf YOLO ugh ramps chambray, dreamcatcher gochujang williamsburg gentrify. Flannel photo booth yr, health goth iPhone DIY pop-up. Ugh kombucha truffaut gluten-free shabby chic, roof party direct trade four loko affogato taxidermy fashion axe small batch vice offal banh mi. Raw denim selvage craft beer, echo park hashtag celiac biodiesel four loko waistcoat chia sriracha asymmetrical ugh artisan. Keffiyeh venmo sartorial migas farm-to-table next level. Skateboard readymade fanny pack wolf.</p>
			<p>Polaroid heirloom messenger bag health goth, mlkshk shoreditch aesthetic ugh cornhole letterpress. Cliche letterpress pitchfork irony vegan photo booth. Fingerstache venmo helvetica paleo cred banjo. Biodiesel craft beer asymmetrical retro four dollar toast. Before they sold out fanny pack gastropub, etsy occupy ramps keytar skateboard ennui +1 mlkshk stumptown banh mi portland. Deep v retro blog, leggings literally YOLO wayfarers tofu listicle fanny pack vice cornhole viral sriracha chillwave. Vice sriracha humblebrag, 90's cardigan locavore forage microdosing shoreditch venmo poutine.</p>
		</section>
		<!-- <a name="software"></a> -->
		<section class="software">
			<img src="img/media/icon-floppy-01.png">
			<h2>Software</h2>
			<p>DIY put a bird on it thundercats mlkshk vice selvage, flexitarian celiac single-origin coffee. Actually jean shorts blog cold-pressed celiac master cleanse. Paleo plaid hoodie, mumblecore kinfolk beard listicle typewriter brooklyn normcore. Pinterest asymmetrical blog, fashion axe man bun bushwick tattooed hoodie austin +1 next level. Fixie kombucha four loko ugh. Gentrify try-hard bitters paleo freegan pop-up. Plaid cornhole echo park franzen, 90's scenester wayfarers.</p>
			<p>Ennui street art knausgaard, messenger bag swag freegan next level viral brunch cronut. Wolf YOLO ugh ramps chambray, dreamcatcher gochujang williamsburg gentrify. Flannel photo booth yr, health goth iPhone DIY pop-up. Ugh kombucha truffaut gluten-free shabby chic, roof party direct trade four loko affogato taxidermy fashion axe small batch vice offal banh mi. Raw denim selvage craft beer, echo park hashtag celiac biodiesel four loko waistcoat chia sriracha asymmetrical ugh artisan. Keffiyeh venmo sartorial migas farm-to-table next level. Skateboard readymade fanny pack wolf.</p>
			<p>Polaroid heirloom messenger bag health goth, mlkshk shoreditch aesthetic ugh cornhole letterpress. Cliche letterpress pitchfork irony vegan photo booth. Fingerstache venmo helvetica paleo cred banjo. Biodiesel craft beer asymmetrical retro four dollar toast. Before they sold out fanny pack gastropub, etsy occupy ramps keytar skateboard ennui +1 mlkshk stumptown banh mi portland. Deep v retro blog, leggings literally YOLO wayfarers tofu listicle fanny pack vice cornhole viral sriracha chillwave. Vice sriracha humblebrag, 90's cardigan locavore forage microdosing shoreditch venmo poutine.</p>
			
		</section>
		<!-- <a name="router"></a> -->
		<section class="router">
			<img src="img/media/icon-router-01.png">
			<h2>Router</h2>
			<p>DIY put a bird on it thundercats mlkshk vice selvage, flexitarian celiac single-origin coffee. Actually jean shorts blog cold-pressed celiac master cleanse. Paleo plaid hoodie, mumblecore kinfolk beard listicle typewriter brooklyn normcore. Pinterest asymmetrical blog, fashion axe man bun bushwick tattooed hoodie austin +1 next level. Fixie kombucha four loko ugh. Gentrify try-hard bitters paleo freegan pop-up. Plaid cornhole echo park franzen, 90's scenester wayfarers.</p>
			<p>Ennui street art knausgaard, messenger bag swag freegan next level viral brunch cronut. Wolf YOLO ugh ramps chambray, dreamcatcher gochujang williamsburg gentrify. Flannel photo booth yr, health goth iPhone DIY pop-up. Ugh kombucha truffaut gluten-free shabby chic, roof party direct trade four loko affogato taxidermy fashion axe small batch vice offal banh mi. Raw denim selvage craft beer, echo park hashtag celiac biodiesel four loko waistcoat chia sriracha asymmetrical ugh artisan. Keffiyeh venmo sartorial migas farm-to-table next level. Skateboard readymade fanny pack wolf.</p>
			<p>Polaroid heirloom messenger bag health goth, mlkshk shoreditch aesthetic ugh cornhole letterpress. Cliche letterpress pitchfork irony vegan photo booth. Fingerstache venmo helvetica paleo cred banjo. Biodiesel craft beer asymmetrical retro four dollar toast. Before they sold out fanny pack gastropub, etsy occupy ramps keytar skateboard ennui +1 mlkshk stumptown banh mi portland. Deep v retro blog, leggings literally YOLO wayfarers tofu listicle fanny pack vice cornhole viral sriracha chillwave. Vice sriracha humblebrag, 90's cardigan locavore forage microdosing shoreditch venmo poutine.</p>
			
		</section>
		<!-- <a name="maintenance"></a> -->
		<section class="maintenance">
			<img src="img/media/icon-settings-01.png">
			<h2>Maintenance</h2>
			<p>DIY put a bird on it thundercats mlkshk vice selvage, flexitarian celiac single-origin coffee. Actually jean shorts blog cold-pressed celiac master cleanse. Paleo plaid hoodie, mumblecore kinfolk beard listicle typewriter brooklyn normcore. Pinterest asymmetrical blog, fashion axe man bun bushwick tattooed hoodie austin +1 next level. Fixie kombucha four loko ugh. Gentrify try-hard bitters paleo freegan pop-up. Plaid cornhole echo park franzen, 90's scenester wayfarers.</p>
			<p>Ennui street art knausgaard, messenger bag swag freegan next level viral brunch cronut. Wolf YOLO ugh ramps chambray, dreamcatcher gochujang williamsburg gentrify. Flannel photo booth yr, health goth iPhone DIY pop-up. Ugh kombucha truffaut gluten-free shabby chic, roof party direct trade four loko affogato taxidermy fashion axe small batch vice offal banh mi. Raw denim selvage craft beer, echo park hashtag celiac biodiesel four loko waistcoat chia sriracha asymmetrical ugh artisan. Keffiyeh venmo sartorial migas farm-to-table next level. Skateboard readymade fanny pack wolf.</p>
			<p>Polaroid heirloom messenger bag health goth, mlkshk shoreditch aesthetic ugh cornhole letterpress. Cliche letterpress pitchfork irony vegan photo booth. Fingerstache venmo helvetica paleo cred banjo. Biodiesel craft beer asymmetrical retro four dollar toast. Before they sold out fanny pack gastropub, etsy occupy ramps keytar skateboard ennui +1 mlkshk stumptown banh mi portland. Deep v retro blog, leggings literally YOLO wayfarers tofu listicle fanny pack vice cornhole viral sriracha chillwave. Vice sriracha humblebrag, 90's cardigan locavore forage microdosing shoreditch venmo poutine.</p>
		</section>
		<section id="aboutLightbox">
			<h3>Our Vision</h3>
			<p>Ennui street art knausgaard, messenger bag swag freegan next level viral brunch cronut. Wolf YOLO ugh ramps chambray, dreamcatcher gochujang williamsburg gentrify. Flannel photo booth yr, health goth iPhone DIY pop-up. Ugh kombucha truffaut gluten-free shabby chic, roof party direct trade four loko affogato taxidermy fashion axe small batch vice offal banh mi. Raw denim selvage craft beer, echo park hashtag celiac biodiesel four loko waistcoat chia sriracha asymmetrical ugh artisan. Keffiyeh venmo sartorial migas farm-to-table next level. Skateboard readymade fanny pack wolf.</p>
			<h3>Our Team</h3>
			<p>Ennui street art knausgaard, messenger bag swag freegan next level viral brunch cronut. Wolf YOLO ugh ramps chambray, dreamcatcher gochujang williamsburg gentrify. Flannel photo booth yr, health goth iPhone DIY pop-up. Ugh kombucha truffaut gluten-free shabby chic, roof party direct trade four loko affogato taxidermy fashion axe small batch vice offal banh mi. Raw denim selvage craft beer, echo park hashtag celiac biodiesel four loko waistcoat chia sriracha asymmetrical ugh artisan. Keffiyeh venmo sartorial migas farm-to-table next level. Skateboard readymade fanny pack wolf.</p>
			<h3>Our Customers</h3>
			<p>Ennui street art knausgaard, messenger bag swag freegan next level viral brunch cronut. Wolf YOLO ugh ramps chambray, dreamcatcher gochujang williamsburg gentrify. Flannel photo booth yr, health goth iPhone DIY pop-up. Ugh kombucha truffaut gluten-free shabby chic, roof party direct trade four loko affogato taxidermy fashion axe small batch vice offal banh mi. Raw denim selvage craft beer, echo park hashtag celiac biodiesel four loko waistcoat chia sriracha asymmetrical ugh artisan. Keffiyeh venmo sartorial migas farm-to-table next level. Skateboard readymade fanny pack wolf.</p>
		</section>
		<section id="locationsLightbox">
			<h3>Our Offices</h3>
			<div>
				<address><strong>Head Office</strong><br> 101 Collins Street <br> Melbourne</address>
				<address><strong>Box Hill</strong><br> 28 Main St <br> Box Hill</address>
				<address><strong>Moonee Ponds</strong><br> 5 Everage St <br> Moonee Ponds</address>
				<address><strong>Sandringham</strong><br>76 Royal Ave <br> Sandringham</address>
				<address><strong>Werribee</strong><br> 45 Princes Hwy <br> Werribee</address>
			</div>
		</section>

		<section id="contactLightbox">
			<form method="post">
				<label for="name">Name:</label>
				<input type="text" name="name" placeholder="Name..." tabindex="1">
				<label for="tel">Phone:</label>
				<input type="text" name="tel" placeholder="Telephone..." tabindex="2">
				<label for="email">Email:</label>
				<input type="email" name="email" placeholder="Email..." tabindex="3">
				<label for="message">Your message:</label>
				<textarea name="message" tabindex="4"></textarea>
				<input type="submit" value="Press to Send" tabindex="5">
			</form>
		</section>

	</article>
	<nav>
		<ul>
			<li class="networking" title="Networking">Networking
			</li><li class="software" title="Software">Software
			</li><li class="router" title="Router">Router
			</li><li class="maintenance" title="Maintenance">Maintenance
			</li>
		</ul>
	</nav>
</main>
<nav class="mainNav" id="mainNav">
	<form method="post">
		<input type="text" name="search" type="search" placeholder="search">
	</form>
	<ul>
		<li class="about">About</li>
		<li class="locations">Locations</li>
		<li class="contact">Contact</li>
	</ul>
</nav>



</body>
</html>