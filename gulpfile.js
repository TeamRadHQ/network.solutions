var gulp         = require('gulp'),
    stylus       = require('gulp-stylus'),
    cleanCSS     = require('gulp-clean-css'),
    beautify     = require('gulp-cssbeautify'),
    prefixer     = require('gulp-autoprefixer'),
    nib          = require('nib'),
    browswerSync = require('browser-sync');

gulp.task('styles', function() {
    gulp.src('css/app.styl')
        .pipe(stylus({ 
        	use: nib() 
        }))
//        .pipe(cleanCSS({
//        	compatibility: 'ie8', 
//        	keepBreaks:    true,
//        }))
		.pipe(prefixer({
			browsers: ['last 2 versions', 'ie 6-8'],
			cascade:  false
		}))
		.pipe(beautify())
        .pipe(gulp.dest('./css/'));
});

gulp.task('watch:styles', function() {
    gulp.watch('**/*.styl', ['styles']);
});

gulp.task('default', ['watch:styles']);